﻿using Assets.Scripts.Unit;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.GameManagement
{
    internal class GameController : MonoBehaviour
    {
        public static event Action OnSceneFinished;
        public static event Action<string> OnWinner;
        
        private void Awake()
        {
            new RoundHandler();
            
            UnitData.OnHealthZero += EndGame;

            Debug.Log($"Start new game");
        }

        private void OnDestroy()
        {
            OnSceneFinished?.Invoke();
            UnitData.OnHealthZero -= EndGame;
        }
        
        private void EndGame(string name)
        {
            if (name == "LeftUnit")
                OnWinner.Invoke("Right Unit");
            else
                OnWinner.Invoke("Left Unit");

            StartCoroutine(StartNewGame());
        }

        private IEnumerator StartNewGame() 
        {
            float delay = 3f;
            yield return new WaitForSeconds(delay);
            SceneManager.LoadScene(0);
            Debug.Log($"Restart");
        }
    }
}
