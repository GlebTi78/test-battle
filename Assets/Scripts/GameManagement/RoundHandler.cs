﻿using Assets.Scripts.UI;
using System;
using UnityEngine;

namespace Assets.Scripts.GameManagement
{
    internal class RoundHandler
    {
        public static event Action OnNextPhase;
        public static event Action<int> OnNextRound;
        
        private int roundCount = 1;
        private GamePhaseType currentGamePhase = GamePhaseType.LeftUnitAttack;

        public RoundHandler()
        {
            UnitPanelController.OnAttackCompleted += NextTurn;
            GameController.OnSceneFinished += Dispose;
        }

        private void Dispose()
        {
            UnitPanelController.OnAttackCompleted -= NextTurn;
            GameController.OnSceneFinished -= Dispose;
        }

        private void NextTurn()
        {
            if (currentGamePhase == GamePhaseType.LeftUnitAttack)
            {
                currentGamePhase = GamePhaseType.RightUnitAttack;
            }
            else if (currentGamePhase == GamePhaseType.RightUnitAttack)
            {
                currentGamePhase = GamePhaseType.LeftUnitAttack;
                roundCount++;
                OnNextRound?.Invoke(roundCount);
                Debug.Log($"New round start = {roundCount}");
            }
            else
            {
                Debug.LogError("Ошибка определения фазы игры.");
            }

            OnNextPhase.Invoke();
            Debug.Log($"New phase start = {currentGamePhase}");
        }
    }
}
