﻿using System;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    internal class UnitData
    {
        public static event Action<string> OnHealthZero;

        private string unitName;
        private int minValue;
        private int maxValue;
        private UnitParameters parameters;

        public UnitData(UnitConfig config, string name) 
        {
            unitName = name;
            minValue = config.MinValue; 
            maxValue = config.MaxValue;

            parameters.Health = config.Health;
            parameters.Armor = config.Armor;
            parameters.VampValue = config.VampValue;
            parameters.Attack = config.Attack;
        }

        public int Health
        {
            get => parameters.Health;
            set
            {
                if (value <= 0)
                    OnHealthZero.Invoke(unitName);
                parameters.Health = value;
            }
        }

        public int Armor
        {
            get => parameters.Armor;
            set => parameters.Armor = Mathf.Clamp(value, minValue, maxValue);
        }

        public int VampValue
        {
            get => parameters.VampValue;
            set => parameters.VampValue = Mathf.Clamp(value, minValue, maxValue);
        }

        public int Attack 
        { 
            get => parameters.Attack; 
            set => parameters.Attack = value; 
        }

        public void UpdateData(UnitParameters data)
        {
            Attack += data.Attack;
            Health += data.Health;
            Armor += data.Armor;
            VampValue += data.VampValue;
        }
    }
}
