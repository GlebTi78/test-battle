﻿using Assets.Scripts.Buffs;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    internal class DamageHandler
    {
        private UnitData unitData;
        
        public DamageHandler (UnitData data) 
        {
            unitData = data;
        }

        public int GetDamage(int value, bool armorDestructionMod)
        {
            int armorTempValue = unitData.Armor;
            if (armorDestructionMod)
            {
                armorTempValue = unitData.Armor - BuffSettins.ArmorDestructionModifier;
                if (armorTempValue < 0)
                    armorTempValue = 0;
            }

            double percentageRatio = 0.01;
            double armorCoeffecient = (100 - armorTempValue) * percentageRatio;
            Debug.Log($"armorCoeffecient = {armorCoeffecient}");
            int damage = (int)(value * armorCoeffecient);

            unitData.Health -= damage;
            return damage;
        }
    }
}
