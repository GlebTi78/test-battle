﻿using Assets.Scripts.Buffs;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    internal class UnitController : MonoBehaviour
    {
        [SerializeField] private UnitConfig config;
        [SerializeField] private string unitName;
        [SerializeField] private UnitPanelController unitPanel;
        [SerializeField] private UnitController target;
        [SerializeField] private SpriteRenderer spriteRenderer;

        private UnitColorHandler colorHandler;
        private UnitData unitData;
        private BuffProvider buffProvider;
        private AttackHandler attackHandler;
        private DamageHandler damageHandler;
        private VampHandler vampHandler;

        public string Name => unitName;

        private void Awake()
        {
            var viewMaterial = spriteRenderer.material;
            
            colorHandler = new UnitColorHandler(viewMaterial, gameObject);
            unitData = new UnitData(config, unitName);
            buffProvider = new BuffProvider(this);
            attackHandler = new AttackHandler(unitName, target);
            damageHandler = new DamageHandler(unitData);
            vampHandler = new VampHandler(unitName, target);
        }

        public void Attack()
        {
            var activeBuffs = buffProvider.GetActiveBuffs();
            int damage = attackHandler.Attack(activeBuffs, unitData.Attack);
            if (damage > 0)
                VampCheck(damage);
        }

        public int GetDamage(int value, bool armorDestructionMod)
        {
            int damage = damageHandler.GetDamage(value, armorDestructionMod);

            if (damage > 0)
                colorHandler.Coloring();
            
            Debug.Log($"{unitName} получил урон = {damage}");
            Debug.Log($"{unitName} параметры: Health = {unitData.Health}, Armor = {unitData.Armor}, Vamp = {unitData.VampValue}");

            unitPanel.UpdateParametersView(unitData.Health, unitData.Armor, unitData.VampValue);
            return damage;
        }

        public void GetBuff()
        {
            buffProvider.GetNewBuff();
        }

        public void BuffsUpdate(UnitParameters data)
        {
            unitData.UpdateData(data);

            Debug.Log($"{unitName}: buffs update");
            Debug.Log($"{unitName} параметры: Health = {unitData.Health}, Armor = {unitData.Armor}, Vamp = {unitData.VampValue}");

            unitPanel.UpdateParametersView(unitData.Health, unitData.Armor, unitData.VampValue);
        }

        public IBuff[] GetActiveBuffs()
        {
            return buffProvider.GetActiveBuffs();
        }

        private void VampCheck(int damag)
        {
            int result = vampHandler.Handler(damag, unitData.VampValue);
            if (result > 0)
            {
                unitData.Health += result;
                unitPanel.UpdateParametersView(unitData.Health, unitData.Armor, unitData.VampValue);
                Debug.Log($"{unitName}: показатель Health увеличен на {result}");
                Debug.Log($"{unitName} параметры: Health = {unitData.Health}, Armor = {unitData.Armor}, Vamp = {unitData.VampValue}");
            }
        }

        public void UpdateView()
        {
            unitPanel.UpdateParametersView(unitData.Health, unitData.Armor, unitData.VampValue);
            unitPanel.UpdateBuffView(buffProvider.GetActiveBuffs());
        }
    }
}
