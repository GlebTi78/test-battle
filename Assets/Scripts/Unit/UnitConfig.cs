﻿using UnityEngine;

namespace Assets.Scripts.Unit
{
    [CreateAssetMenu(fileName = "UnitConfig", menuName = "Data/Unit")]
    internal class UnitConfig : ScriptableObject
    {
        [SerializeField] private int maxValue;
        [SerializeField] private int minValue;
        [SerializeField] private int health;
        [SerializeField] private int armor;
        [SerializeField] private int vampValue;
        [SerializeField] private int attack;
        
        public int MaxValue => maxValue;
        public int MinValue => minValue;
        public int Health => health;
        public int Armor => armor;
        public int VampValue => vampValue;
        public int Attack => attack;
    }
}
