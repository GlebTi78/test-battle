﻿namespace Assets.Scripts.Unit
{
    internal struct UnitParameters
    {
        public int Attack;
        public int Health;
        public int Armor;
        public int VampValue;
    }
}
