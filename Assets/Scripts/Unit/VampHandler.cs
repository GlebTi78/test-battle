﻿using Assets.Scripts.Buffs;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    internal class VampHandler
    {
        private string unitName;
        private UnitController target;

        public VampHandler(string name, UnitController target)
        {
            unitName = name;
            this.target = target;
        }

        public int Handler(int damagValue, int vampValue)
        {
            if (vampValue == 0)
            {
                Debug.Log($"{unitName}: Vampirism default = 0");
                return 0;
            }

            int vampTempValue = vampValue;
            bool isVampirismDecreaseBuffActive = VampirismDecreaseCheck();

            if (isVampirismDecreaseBuffActive)
                vampTempValue -= BuffSettins.VampirismDecreaseModifier;

            if (vampTempValue <= 0)
            {
                Debug.Log($"{unitName}: Vampirism result = 0");
                return 0;
            }

            Debug.Log($"{unitName}: Vampirism result = {vampTempValue}");

            return Calculate(damagValue, vampTempValue);
        }
        
        private bool VampirismDecreaseCheck()
        {
            var targetActiveBuffs = target.GetActiveBuffs();

            foreach (var buff in targetActiveBuffs)
            {
                if (buff?.GetBuffType() == BuffType.VampirismDecrease)
                {
                    Debug.Log($"У противника активирован бафф {BuffType.VampirismDecrease}");
                    return true;
                }
            }

            return false;
        }

        private int Calculate(int damageValue, int vampValue)
        {
            double percentageRatio = 0.01;
            return (int)(damageValue * vampValue * percentageRatio);
        }
    }
}
