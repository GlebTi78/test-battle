﻿using Assets.Scripts.Buffs;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    internal class AttackHandler
    {
        private string unitName;
        private UnitController target;

        public AttackHandler(string name, UnitController target)
        {
            unitName = name;
            this.target = target;
        }

        public int Attack(IBuff[] activeBuffs, int attackPower)
        {
            bool armorDestructionMod = false;
            
            foreach (var buff in activeBuffs)
            {
                if (buff?.GetBuffType() == BuffType.ArmorDestruction)
                {
                    armorDestructionMod = true;
                    break;
                }
            }
            
            if (armorDestructionMod)
                Debug.Log($"ArmorDestruction применен");

            Debug.Log($"{unitName} атаковал с силой атаки = {attackPower}");
            
            return target.GetDamage(attackPower, armorDestructionMod);
        }
    }
}
