﻿using Assets.Scripts.GameManagement;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    internal class TopPanelController : MonoBehaviour
    {
        [SerializeField] private Button restartButton;
        [SerializeField] private TMP_Text roundText;
        [SerializeField] private GameObject resultGamePanel;
        [SerializeField] private TMP_Text resultGameText;

        private void Awake()
        {
            restartButton.onClick.AddListener(RestartGame);
            RoundHandler.OnNextRound += UpdateRoundText;
            GameController.OnWinner += OpenResultGamePanel;
        }

        private void OnDestroy()
        {
            restartButton.onClick.RemoveAllListeners();
            RoundHandler.OnNextRound -= UpdateRoundText;
            GameController.OnWinner -= OpenResultGamePanel;
        }

        private void RestartGame()
        {
            SceneManager.LoadScene(0);
            Debug.Log($"Restart game");
        }

        private void UpdateRoundText(int value)
        {
            roundText.text = $"Round: {value}";
        }

        private void OpenResultGamePanel(string winnerName)
        {
            resultGamePanel.SetActive(true);
            resultGameText.text = $"Победитель - {winnerName}";
        }
    }
}
