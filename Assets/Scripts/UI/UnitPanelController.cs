﻿using Assets.Scripts.Buffs;
using Assets.Scripts.GameManagement;
using System;
using UnityEngine;

namespace Assets.Scripts.UI
{
    internal class UnitPanelController : MonoBehaviour
    {
        public static event Action OnAttackCompleted;

        private UnitPanelConfig config;
        private UnitPanelView panelView;

        private void Awake()
        {
            config = GetComponent<UnitPanelConfig>();
            
            panelView = new UnitPanelView(config.HealthText, config.ArmorText, config.VampText, config.BuffText);
            
            RoundHandler.OnNextPhase += ActivatedButtons;
            config.AttackButton.onClick.AddListener(Attack);
            config.BuffButton.onClick.AddListener(GetBuff);

            ActivatedButtons();
        }

        private void OnDestroy()
        {
            RoundHandler.OnNextPhase -= ActivatedButtons;
            config.AttackButton.onClick.RemoveAllListeners();
            config.BuffButton.onClick.RemoveAllListeners();
        }

        private void ActivatedButtons()
        {
            config.AttackButton.enabled = config.IsActive;
            config.BuffButton.enabled = config.IsActive;

            config.IsActive = !config.IsActive;
        }

        private void Attack()
        {
            config.Unit.Attack();
            OnAttackCompleted.Invoke();
        }

        private void GetBuff()
        {
            config.Unit.GetBuff();
            config.BuffButton.enabled = false;
        }

        public void UpdateParametersView(int health, int armor, int vamp)
        {
            panelView.UpdateParametersView(health, armor, vamp);
        }

        public void UpdateBuffView(IBuff[] activeBuffs)
        {
            string listBuffs = "";
            foreach (IBuff buff in activeBuffs)
            {
                if (buff != null)
                {
                    string name = buff.GetName();
                    int duration = buff.Duration;
                    listBuffs = $"{listBuffs}{name} ({duration})\n";
                }
            }
            
            panelView.UpdateBuffView(listBuffs);
        }
    }
}
