﻿using Assets.Scripts.Unit;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    internal class UnitPanelConfig : MonoBehaviour
    {
        public bool IsActive;

        [SerializeField] private UnitController unit;
        [SerializeField] private Button attackButton;
        [SerializeField] private Button buffButton;
        [SerializeField] private TMP_Text healthText;
        [SerializeField] private TMP_Text armorText;
        [SerializeField] private TMP_Text vampText;
        [SerializeField] private TMP_Text buffText;

        public UnitController Unit => unit;
        public Button AttackButton => attackButton;
        public Button BuffButton => buffButton;
        public TMP_Text HealthText => healthText;
        public TMP_Text ArmorText => armorText;
        public TMP_Text VampText => vampText;
        public TMP_Text BuffText => buffText;
    }
}
