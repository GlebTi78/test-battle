﻿using TMPro;

namespace Assets.Scripts.UI
{
    internal class UnitPanelView
    {
        private TMP_Text healthText;
        private TMP_Text armorText;
        private TMP_Text vampText;
        private TMP_Text buffText;

        public UnitPanelView(TMP_Text health, TMP_Text armor, TMP_Text vamp, TMP_Text buff)
        {
            healthText = health;
            armorText = armor;
            vampText = vamp;
            buffText = buff;
        }

        public void UpdateParametersView(float health, int armor, int vamp)
        {
            healthText.text = $"Health: {health}";
            armorText.text = $"Armor: {armor}";
            vampText.text = $"Vamp: {vamp}";
        }

        public void UpdateBuffView(string listBuffs)
        {
            buffText.text = "ActiveBuffs:\n" + listBuffs;
        }
    }
}
