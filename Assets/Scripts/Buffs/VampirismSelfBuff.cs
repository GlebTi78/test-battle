﻿using Assets.Scripts.Unit;

namespace Assets.Scripts.Buffs
{
    internal class VampirismSelfBuff : BaseBuff, IBuff
    {
        public VampirismSelfBuff(UnitController unit) : base(unit)
        {
            Name = "Vampirism self";
            BuffType = BuffType.VampirismSelf;
            MaxDuration = BuffSettins.VampirismSelfDuration;
            Duration = MaxDuration;
            Parameters.Armor = BuffSettins.VampirismSelfArmorModifier;
            Parameters.VampValue = BuffSettins.VampirismSelfVampModifier;
        }

        public override void CancelBuff()
        {
            base.CancelBuff();
            Parameters.VampValue = -BuffSettins.VampirismSelfVampModifier;
            Unit.BuffsUpdate(Parameters);
        }
    }
}
