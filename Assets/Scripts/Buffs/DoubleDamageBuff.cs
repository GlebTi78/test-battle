﻿using Assets.Scripts.Unit;

namespace Assets.Scripts.Buffs
{
    internal class DoubleDamageBuff : BaseBuff, IBuff
    {
        public DoubleDamageBuff(UnitController unit) : base(unit)
        {
            Name = "Double damage";
            BuffType = BuffType.DoubleDamage;
            MaxDuration = BuffSettins.DoubleDamageDuration;
            Duration = MaxDuration;
            Parameters.Attack = BuffSettins.DoubleDamageModifier;
        }

        public override void CancelBuff()
        {
            base.CancelBuff();
            Parameters.Attack = -BuffSettins.DoubleDamageModifier;
            Unit.BuffsUpdate(Parameters);
        }
    }
}
