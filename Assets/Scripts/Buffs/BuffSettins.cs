﻿namespace Assets.Scripts.Buffs
{
    internal struct BuffSettins
    {
        public static readonly int DoubleDamageModifier = 15;
        public static readonly int ArmorSelfModifier = 50;
        public static readonly int ArmorDestructionModifier = 10;
        public static readonly int VampirismSelfArmorModifier = -25;
        public static readonly int VampirismSelfVampModifier = 50;
        public static readonly int VampirismDecreaseModifier = 25;
        
        public static readonly int DoubleDamageDuration = 3;
        public static readonly int ArmorSelfDuration = 2;
        public static readonly int ArmorDestructionDuration = 2;
        public static readonly int VampirismSelfDuration = 1;
        public static readonly int VampirismDecreaseDuration = 2;
    }
}
