﻿using Assets.Scripts.GameManagement;
using Assets.Scripts.Unit;

namespace Assets.Scripts.Buffs
{
    internal class BuffProvider
    {
        private IBuff[] allBuffs;
        private IBuff[] activeBuffs;
        private int activeBuffsMaxValue = 2;
        private AddBuffHandler addBuffHandler;
        private UnitController unit;

        public BuffProvider(UnitController unit)
        {
            this.unit = unit;
            activeBuffs = new IBuff[activeBuffsMaxValue];

            allBuffs = new IBuff[]
            {
                new DoubleDamageBuff(unit),
                new ArmorSelfBuff(unit),
                new ArmorDestructionBuff(unit),
                new VampirismSelfBuff(unit),
                new VampirismDecreaseBuff(unit)
            };

            addBuffHandler = new AddBuffHandler(allBuffs, activeBuffs, activeBuffsMaxValue);

            addBuffHandler.OnBuff += AddBuffCompleted;
            RoundHandler.OnNextRound += UpdateActiveBuffs;
            GameController.OnSceneFinished += Dispose;
        }

        private void Dispose()
        {
            addBuffHandler.OnBuff -= AddBuffCompleted;
            RoundHandler.OnNextRound -= UpdateActiveBuffs;
            GameController.OnSceneFinished -= Dispose;
        }

        public void GetNewBuff()
        {
            addBuffHandler.GetBuff();
        }

        public IBuff[] GetActiveBuffs()
        {
            return activeBuffs;
        }

        private void AddBuffCompleted()
        {
            unit.UpdateView();
        }

        private void UpdateActiveBuffs(int value)
        {
            for (int i = 0; i < activeBuffs.Length; i++)
            {
                if (activeBuffs[i] == null)
                    continue;

                activeBuffs[i].Duration--;
                if (activeBuffs[i].Duration == 0)
                {
                    activeBuffs[i].CancelBuff();
                    activeBuffs[i] = null;
                }
            }

            unit.UpdateView();
        }
    }
}
