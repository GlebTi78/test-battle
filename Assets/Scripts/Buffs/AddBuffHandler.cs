﻿using System;
using UnityEngine;

namespace Assets.Scripts.Buffs
{
    internal class AddBuffHandler
    {
        public event Action OnBuff;
        
        private IBuff[] allBuffs;
        private IBuff[] activeBuffs;
        private int activeBuffsMaxValue;

        public AddBuffHandler(IBuff[] allBuffs, IBuff[] activeBuffs, int value) 
        {
            this.allBuffs = allBuffs;
            this.activeBuffs = activeBuffs;
            activeBuffsMaxValue = value;
        }

        public void GetBuff()
        {
            byte activeBuffCount = 0;
            BuffType exclusionBuffType = BuffType.None;

            for (int i = 0; i < activeBuffsMaxValue; i++)
            {
                if (activeBuffs[i] != null)
                {
                    activeBuffCount++;
                    exclusionBuffType = activeBuffs[i].GetBuffType();
                }
            }

            if (activeBuffCount == activeBuffsMaxValue)
            {
                Debug.Log($"Невозможно получить новый бафф. Все слоты баффов заполнены.");
                return;
            }
            else if (activeBuffCount < activeBuffsMaxValue)
            {
                AddBuffInList(exclusionBuffType);
            }
            else
            {
                Debug.LogError($"Ошибка алгоритма BuffHandler.GetBuff()");
            }
        }

        private IBuff RandomizeBuff(BuffType exclusionBuffType)
        {
            int index = UnityEngine.Random.Range(0, allBuffs.Length);
            IBuff buff = allBuffs[index];
            
            if (buff.GetBuffType() == exclusionBuffType)
                buff = RandomizeBuff(exclusionBuffType);

            return buff;
        }

        private void AddBuffInList(BuffType exclusionBuffType)
        {
            var buff = RandomizeBuff(exclusionBuffType);
            for (int i = 0; i < activeBuffsMaxValue; i++)
            {
                if (activeBuffs[i] == null)
                {
                    activeBuffs[i] = buff;
                    activeBuffs[i].Handler();
                    OnBuff.Invoke();
                    
                    Debug.Log($"Добавлен бафф {buff.GetBuffType()}");
                    return;
                }
            }
        }
    }
}
