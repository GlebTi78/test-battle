﻿namespace Assets.Scripts.Buffs
{
    internal enum BuffType
    {
        None,
        DoubleDamage,
        ArmorSelf,
        ArmorDestruction,
        VampirismSelf,
        VampirismDecrease
    }
}
