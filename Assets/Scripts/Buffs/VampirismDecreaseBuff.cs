﻿using Assets.Scripts.Unit;
using UnityEngine;

namespace Assets.Scripts.Buffs
{
    internal class VampirismDecreaseBuff : BaseBuff, IBuff
    {
        public VampirismDecreaseBuff(UnitController unit) : base(unit)
        {
            Name = "Vampirism decrease";
            BuffType = BuffType.VampirismDecrease;
            MaxDuration = BuffSettins.VampirismDecreaseDuration;
            Duration = MaxDuration;
        }

        public override void Handler()
        {
            Debug.Log($"Бафф {Name} получен и будет применен при защите");
        }
    }
}
