﻿using Assets.Scripts.Unit;

namespace Assets.Scripts.Buffs
{
    internal class ArmorSelfBuff : BaseBuff, IBuff
    {
        public ArmorSelfBuff(UnitController unit) : base(unit)
        {
            Name = "Armor self";
            BuffType = BuffType.ArmorSelf;
            MaxDuration = BuffSettins.ArmorSelfDuration;
            Duration = MaxDuration;
            Parameters.Armor = BuffSettins.ArmorSelfModifier;
        }

        public override void CancelBuff()
        {
            base.CancelBuff();
            Parameters.Armor = -BuffSettins.ArmorSelfModifier;
            Unit.BuffsUpdate(Parameters);
        }
    }
}
