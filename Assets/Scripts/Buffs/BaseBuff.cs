﻿using Assets.Scripts.Unit;
using UnityEngine;

namespace Assets.Scripts.Buffs
{
    internal abstract class BaseBuff
    {
        protected string Name;
        protected BuffType BuffType;
        protected int MaxDuration;
        protected UnitController Unit;
        protected UnitParameters Parameters;
        public int Duration { get; set; }

        public BaseBuff(UnitController unit)
        {
            Unit = unit;
        }

        public string GetName()
        {
            return Name;
        }
        
        public BuffType GetBuffType()
        {
            return BuffType;
        }

        public virtual void Handler()
        {
            Unit.BuffsUpdate(Parameters);
            Debug.Log($"Применен бафф {Name}");
        }

        public virtual void CancelBuff()
        {
            Debug.Log($"Бафф {Name} деактивирован");
            Duration = MaxDuration;
        }
    }
}
