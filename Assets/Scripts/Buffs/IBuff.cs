﻿namespace Assets.Scripts.Buffs
{
    internal interface IBuff
    {
        int Duration { get; set; }
        string GetName();
        BuffType GetBuffType();
        void Handler();
        void CancelBuff();
    }
}
