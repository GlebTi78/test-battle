﻿using Assets.Scripts.Unit;
using UnityEngine;

namespace Assets.Scripts.Buffs
{
    internal class ArmorDestructionBuff : BaseBuff, IBuff
    {
        public ArmorDestructionBuff(UnitController unit) : base(unit)
        {
            Name = "Armor destruction";
            BuffType = BuffType.ArmorDestruction;
            MaxDuration = BuffSettins.ArmorDestructionDuration;
            Duration = MaxDuration;
        }

        public override void Handler()
        {
            Debug.Log($"Бафф {Name} получен и будет применен при атаке");
        }
    }
}
